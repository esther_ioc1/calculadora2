
import java.util.Scanner;
/**
 * @author Esther Pont Martori
 * 
 */
public class Calculadora {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
         
        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        double suma = num1 + num2;
        System.out.println("Suma: " + suma);
        
        double resta = num1 - num2;
        System.out.println("Resta: " + resta);
        
        double multiplicacio = num1 * num2;
        System.out.println("Multiplicació: " + multiplicacio);
        
        if (num2 == 0) {
            throw new ArithmeticException("El divisor no pot ser zero");
        } else {
            double divisio = num1 / num2;
            System.out.println("Divisió: " + divisio);
        }
    }
    
}
